flask==1.1.2
flask_migrate==2.5.3
flask_restx==0.2.0
flask-sqlalchemy==2.4.4
psycopg2-binary==2.8.6
pytest==6.1.2
