"""
1. reserve ticket
2. cannot reserve than in the pool
3. ticket back to pool after reservation is expired

"""
import pytest

from project import db
from project.model import TicketPool, EmptyTicketPoolError
from project.tests.conftest import FAKE_NOW, RESERVATION_EXPIRATION_PERIOD


def test_decrement_available_tickets_upon_reservation(
    test_app, test_database, add_event, add_ticket_pool, mock_datetime_now
):
    event = add_event(name="GiftsDelivery", datetime=FAKE_NOW)
    ticket_pool = add_ticket_pool(
        event, ticket_type="regular", available_tickets=1, importance=1
    )
    ticket_pool.reserve_ticket()
    db.session.commit()
    assert ticket_pool.available_tickets == 0
    assert len(ticket_pool.reservations) == 1


def test_raise_exception_when_no_available_tickets_upon_reservation(
    test_app, test_database, add_event, add_ticket_pool, mock_datetime_now
):
    ticket_pool = TicketPool.query.first()
    assert ticket_pool.available_tickets == 0
    with pytest.raises(EmptyTicketPoolError):
        ticket_pool.reserve_ticket()


def test_release_expired_reservations_upon_reservation(
    test_app, test_database, add_event, add_ticket_pool, mock_datetime_now
):
    ticket_pool = TicketPool.query.first()
    assert ticket_pool.available_tickets == 0
    assert len(ticket_pool.reservations) == 1
    mock_datetime_now.now.return_value = FAKE_NOW + RESERVATION_EXPIRATION_PERIOD
    ticket_pool.reserve_ticket()
    assert ticket_pool.available_tickets == 0
    assert len(ticket_pool.reservations) == 1
