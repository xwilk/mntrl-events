import datetime as dt

from project.model import TicketPool, Reservation
from project.tests.conftest import FAKE_NOW, RESERVATION_EXPIRATION_PERIOD


def test_reservation_is_valid_for_15_minutes(
    test_app, test_database, mock_datetime_now
):
    reservation = Reservation(TicketPool())
    mock_datetime_now.now.return_value = FAKE_NOW + dt.timedelta(minutes=15)
    assert not reservation.has_expired()


def test_reservation_expires_after_15_minutes_and_1_second(
    test_app, test_database, mock_datetime_now
):
    reservation = Reservation(TicketPool())
    mock_datetime_now.now.return_value = FAKE_NOW + dt.timedelta(minutes=15, seconds=1)
    assert reservation.has_expired()
