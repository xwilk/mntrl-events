import datetime

import pytest
from sqlalchemy.exc import IntegrityError

from project.tests.conftest import FAKE_NOW


def test_event_has_name_date_and_time(test_app, test_database, add_event):
    dt = datetime.datetime(year=2020, month=12, day=6, hour=15, minute=46)
    event = add_event(name="GiftsDelivery", datetime=dt)
    assert str(event.datetime.date()) == "2020-12-06"
    assert str(event.datetime.time()) == "15:46:00"


def test_event_can_have_multiple_ticket_types_ordered_by_importance(
    test_app, test_database, add_event, add_ticket_pool
):
    event = add_event(name="BoringEvent", datetime=FAKE_NOW)
    add_ticket_pool(event, ticket_type="regular", available_tickets=1, importance=1)
    add_ticket_pool(event, ticket_type="vip", available_tickets=1, importance=99)
    add_ticket_pool(event, ticket_type="premium", available_tickets=1, importance=20)
    assert tuple(pool.type for pool in event.ticket_pools) == (
        "vip",
        "premium",
        "regular",
    )


def test_event_ticket_pool_can_have_multiple_available_tickets(
    test_app, test_database, add_event, add_ticket_pool
):
    event = add_event(name="VeryBoringEvent", datetime=FAKE_NOW)
    add_ticket_pool(event, ticket_type="regular", available_tickets=4, importance=1)
    assert len(event.ticket_pools) == 1
    assert event.ticket_pools[0].available_tickets == 4


def test_event_name_is_unique(test_app, test_database, add_event, add_ticket_pool):
    add_event(name="DoppelgängerFest", datetime=FAKE_NOW)
    with pytest.raises(IntegrityError):
        add_event(name="DoppelgängerFest", datetime=FAKE_NOW)
