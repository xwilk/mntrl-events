import datetime
from unittest.mock import MagicMock

import pytest

from project import create_app, db
from project.model import Event, TicketPool, Reservation

FAKE_NOW = datetime.datetime(2040, 1, 1, 12, 0, 0)
RESERVATION_EXPIRATION_PERIOD = datetime.timedelta(minutes=15, seconds=1)


@pytest.fixture(scope="module")
def test_app():
    app = create_app()
    app.config.from_object("project.config.TestingConfig")
    with app.app_context():
        yield app


@pytest.fixture(scope="module")
def test_database():
    db.create_all()
    yield db
    db.session.remove()
    db.drop_all()


@pytest.fixture(scope="function")
def add_event():
    def _add_event(name, datetime):
        event = Event(name=name, datetime=datetime)
        db.session.add(event)
        db.session.commit()
        return event

    return _add_event


@pytest.fixture(scope="function")
def add_ticket_pool():
    def _add_ticket_pool(event, ticket_type, available_tickets, importance=1):
        ticket_pool = TicketPool(
            event=event,
            type=ticket_type,
            available_tickets=available_tickets,
            importance=importance,
        )
        db.session.add(ticket_pool)
        db.session.commit()
        return ticket_pool

    return _add_ticket_pool


@pytest.fixture(scope="function")
def mock_datetime_now(monkeypatch):
    datetime_mock = MagicMock(wraps=datetime.datetime)
    datetime_mock.now.return_value = FAKE_NOW
    monkeypatch.setattr(datetime, "datetime", datetime_mock)
    yield datetime_mock


def reserve_ticket_from_existing_pool():
    ticket_pool = TicketPool.query.first()
    assert ticket_pool is not None
    reservation = ticket_pool.reserve_ticket()
    db.session.commit()
    return reservation
