"""
1. change route to /reservation/<id>/payment
2. test token card error
3. test token payment error
4. test unsupported currency
5. test invalid input
"""

import json

import pytest

from project.tests.conftest import (
    FAKE_NOW,
    RESERVATION_EXPIRATION_PERIOD,
    reserve_ticket_from_existing_pool,
)

PAYMENT_URL = "/event/reservation/{reservation_id}/payment"


def test_user_can_pay_for_reservation(
    test_app,
    test_database,
    add_event,
    add_ticket_pool,
    mock_datetime_now,
):
    e1 = add_event(name="Event-1", datetime=FAKE_NOW)
    add_ticket_pool(e1, ticket_type="regular", available_tickets=100, importance=1)
    reservation = reserve_ticket_from_existing_pool()
    client = test_app.test_client()

    resp = client.post(
        PAYMENT_URL.format(reservation_id=reservation.id),
        data=json.dumps(
            {
                "amount": 15,
                "token": "valid",
                "currency": "EUR",
            }
        ),
        content_type="application/json",
    )

    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert data["amount"] == 15
    assert data["currency"] == "EUR"


def test_user_cannot_pay_for_nonexistent_reservation(
    test_app, test_database, mock_datetime_now
):
    client = test_app.test_client()

    resp = client.post(
        PAYMENT_URL.format(reservation_id=9999),
        data=json.dumps(
            {
                "amount": 15,
                "token": "valid",
                "currency": "EUR",
            }
        ),
        content_type="application/json",
    )

    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert resp.content_type == "application/json"
    assert "Reservation is not valid!" in data["message"]


def test_user_cannot_pay_for_the_reservation_twice(
    test_app, test_database, mock_datetime_now
):
    reservation = reserve_ticket_from_existing_pool()
    client = test_app.test_client()

    payload = {"amount": 15, "token": "valid", "currency": "EUR"}
    resp = client.post(
        PAYMENT_URL.format(reservation_id=reservation.id),
        data=json.dumps(payload),
        content_type="application/json",
    )

    assert resp.status_code == 200

    resp = client.post(
        PAYMENT_URL.format(reservation_id=reservation.id),
        data=json.dumps(payload),
        content_type="application/json",
    )

    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert resp.content_type == "application/json"
    assert "Reservation is not valid!" in data["message"]


@pytest.mark.parametrize(
    "payload",
    [
        {"amount": 15, "token": "card_error", "currency": "EUR"},
        {"amount": 15, "token": "payment_error", "currency": "EUR"},
        {"amount": 15, "token": "payment_error", "currency": "PLN"},
    ],
)
def test_payment_failures(test_app, test_database, payload, mock_datetime_now):
    reservation = reserve_ticket_from_existing_pool()
    client = test_app.test_client()

    resp = client.post(
        PAYMENT_URL.format(reservation_id=reservation.id),
        data=json.dumps(payload),
        content_type="application/json",
    )

    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert resp.content_type == "application/json"
    assert "Payment error: " in data["message"]


def test_reservation_expires_during_payment(test_app, test_database, mock_datetime_now):
    reservation = reserve_ticket_from_existing_pool()
    client = test_app.test_client()

    mock_datetime_now.now.return_value = FAKE_NOW + RESERVATION_EXPIRATION_PERIOD

    resp = client.post(
        PAYMENT_URL.format(reservation_id=reservation.id),
        data=json.dumps(
            {
                "amount": 15,
                "token": "valid",
                "currency": "EUR",
            }
        ),
        content_type="application/json",
    )

    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert resp.content_type == "application/json"
    assert "Reservation has expired!" in data["message"]


@pytest.mark.parametrize(
    "payload",
    [
        # missing argument
        {"amount": 15, "token": "valid"},
        {"amount": 15, "currency": "EUR"},
        {"token": "valid", "currency": "EUR"},
        # wrong types
        {"amount": 15, "token": "valid", "currency": 4},
        {"amount": 15, "token": 4, "currency": "EUR"},
        {"amount": "4", "token": "valid", "currency": "EUR"},
    ],
)
def test_invalid_json(test_app, test_database, payload):
    client = test_app.test_client()
    resp = client.post(
        PAYMENT_URL.format(reservation_id=9999),
        data=json.dumps(payload),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert resp.content_type == "application/json"
    assert "Input payload validation failed" in data["message"]
