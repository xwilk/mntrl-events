import datetime
import json

EVENT_URL = "/event"


def test_user_can_get_full_event_list(test_app, test_database, add_event):
    add_event(name="Event-1", datetime=datetime.datetime.today())
    add_event(name="Event-2", datetime=datetime.datetime.today())
    client = test_app.test_client()
    resp = client.get(EVENT_URL, follow_redirects=True)
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data) == 2
    assert data[0]["name"] == "Event-1"
    assert data[1]["name"] == "Event-2"


def test_user_can_get_specific_event_by_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.get(f"{EVENT_URL}/1")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["name"] == "Event-1"


def test_user_will_receive_404_when_event_does_not_exists(test_app, test_database):
    client = test_app.test_client()
    resp = client.get(f"{EVENT_URL}/99")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert resp.content_type == "application/json"
    assert "Event does not exist." in data["message"]
