import datetime
import json

import pytest

RESERVATION_URL = "/event/reservation"


def test_user_can_reserve_a_ticket_from_given_ticket_pool(
    test_app, test_database, add_event, add_ticket_pool, mock_datetime_now
):
    e1 = add_event(name="Event-1", datetime=datetime.datetime.today())
    add_ticket_pool(e1, ticket_type="regular", available_tickets=1, importance=1)
    client = test_app.test_client()
    resp = client.post(
        RESERVATION_URL,
        data=json.dumps({"event": "Event-1", "type": "regular"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    reservation = e1.ticket_pools[0].reservations[0]
    assert data["id"] == reservation.id
    assert data["expiration_time"] == reservation.expiration_time.isoformat()


@pytest.mark.parametrize(
    "payload",
    [
        {},
        {"event": "Event-1"},
        {"type": "regular"},
    ],
)
def test_user_has_to_pass_valid_json_keys_for_ticket_reservation(
    test_app, test_database, payload
):
    client = test_app.test_client()
    resp = client.post(
        RESERVATION_URL, data=json.dumps(payload), content_type="application/json"
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert resp.content_type == "application/json"
    assert "Input payload validation failed" in data["message"]


@pytest.mark.parametrize(
    "payload",
    [
        {"event": "wrong event name", "type": "regular"},
        {"event": "Event-1", "type": "wrong ticket type"},
    ],
)
def test_user_has_to_pass_existing_event_and_ticket_type(
    test_app, test_database, payload
):
    client = test_app.test_client()
    resp = client.post(
        RESERVATION_URL, data=json.dumps(payload), content_type="application/json"
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert resp.content_type == "application/json"
    assert "Invalid event or ticket type" in data["message"]


def test_user_cannot_reserve_ticket_from_empty_pool(test_app, test_database):
    client = test_app.test_client()
    ticket_type = "regular"
    resp = client.post(
        RESERVATION_URL,
        data=json.dumps({"event": "Event-1", "type": ticket_type}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert resp.content_type == "application/json"
    assert (
        f"{ticket_type.capitalize()} tickets are no longer available!"
        in data["message"]
    )
