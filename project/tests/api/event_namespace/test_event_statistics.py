import json

from project import db
from project.model import Event, TicketPool
from project.tests.conftest import FAKE_NOW

STATISTICS_URL = "/event/statistics"


def add_event(name, datetime):
    event = Event(name=name, datetime=datetime)
    db.session.add(event)
    return event


def add_ticket_pool(event, ticket_type, available_tickets, importance=1):
    ticket_pool = TicketPool(
        event=event,
        type=ticket_type,
        available_tickets=available_tickets,
        importance=importance,
    )
    db.session.add(ticket_pool)
    return ticket_pool


def test_user_can_see_amount_of_reserved_tickets_for_each_event(
    test_app, test_database
):
    event_1 = add_event("Event-1", datetime=FAKE_NOW)
    event_2 = add_event("Event-2", datetime=FAKE_NOW)
    pool_1 = add_ticket_pool(
        event_1, ticket_type="regular", available_tickets=100, importance=1
    )
    pool_2 = add_ticket_pool(
        event_2, ticket_type="regular", available_tickets=100, importance=1
    )
    pool_3 = add_ticket_pool(
        event_2, ticket_type="vip", available_tickets=10, importance=100
    )
    pool_1.reserve_ticket()
    pool_2.reserve_ticket()
    pool_3.reserve_ticket()
    db.session.commit()

    client = test_app.test_client()
    resp = client.get(STATISTICS_URL, content_type="application/json")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"

    expected = [
        {"event": "Event-1", "ticket_type": "all", "reservations": 1},
        {"event": "Event-2", "ticket_type": "all", "reservations": 2},
    ]

    assert data == expected


def test_user_can_see_amount_of_reserved_tickers_for_specified_type_for_each_event(
    test_app, test_database
):
    client = test_app.test_client()
    resp = client.get(
        STATISTICS_URL,
        data=json.dumps({"ticket_type": "regular"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"

    expected = [
        {"event": "Event-1", "ticket_type": "regular", "reservations": 1},
        {"event": "Event-2", "ticket_type": "regular", "reservations": 1},
    ]

    assert data == expected
