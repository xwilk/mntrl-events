import datetime
import json

from project.tests.conftest import reserve_ticket_from_existing_pool

RESERVATION_URL = "/event/reservation/{reservation_id}"


def test_user_can_see_reservation_details(
    test_app, test_database, add_event, add_ticket_pool, mock_datetime_now
):
    e1 = add_event(name="Event-1", datetime=datetime.datetime.today())
    add_ticket_pool(e1, ticket_type="regular", available_tickets=1, importance=1)
    reservation = reserve_ticket_from_existing_pool()
    client = test_app.test_client()
    resp = client.get(
        RESERVATION_URL.format(reservation_id=reservation.id),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert data["id"] == reservation.id
    assert data["expiration_time"] == reservation.expiration_time.isoformat()


def test_user_will_receive_404_when_reservation_does_not_exists(
    test_app, test_database
):
    client = test_app.test_client()
    resp = client.get(
        RESERVATION_URL.format(reservation_id=9999),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert resp.content_type == "application/json"
    assert "Reservation does not exist." in data["message"]
