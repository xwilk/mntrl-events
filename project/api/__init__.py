from flask_restx import Api

from project.api.ping import ping_namespace
from project.api.events import events_namespace

api = Api(version="1.0", title="Mntrl Event API", doc="/doc/")
api.add_namespace(ping_namespace, path="/ping")
api.add_namespace(events_namespace, path="/event")
