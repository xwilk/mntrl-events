from flask import request
from flask_restx import Namespace, Resource, fields
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

from project import db, model
from project.core.event_statistics import get_event_statistics
from project.core.payment_gateway import (
    PaymentGateway,
    CardError,
    CurrencyError,
    PaymentError,
)

events_namespace = Namespace("event")

reservation_model = events_namespace.model(
    "Reservation",
    {
        "id": fields.Integer(readOnly=True),
        "expiration_time": fields.DateTime(required=True),
    },
)

ticket_pool_model = events_namespace.model(
    "TicketPool",
    {
        "id": fields.Integer(readOnly=True),
        "type": fields.String(required=True),
        "available_tickets": fields.Integer(required=True),
        "reservations": fields.Nested(reservation_model, required=True),
    },
)

event_model = events_namespace.model(
    "Event",
    {
        "id": fields.Integer(readOnly=True),
        "name": fields.String(required=True),
        "datetime": fields.DateTime(required=True),
        "ticket_pools": fields.Nested(ticket_pool_model, required=True),
    },
)


class EventList(Resource):
    @events_namespace.marshal_with(event_model, as_list=True)
    def get(self):
        return model.Event.query.all()


class Event(Resource):
    @events_namespace.marshal_with(event_model)
    def get(self, event_id):
        event = model.Event.query.filter_by(id=event_id).first()
        if event is None:
            events_namespace.abort(404, "Event does not exist.")
        return event


reservation_input = events_namespace.model(
    "ReservationInput",
    {
        "event": fields.String(required=True),
        "type": fields.String(required=True),
    },
)


class ReservationsList(Resource):
    @events_namespace.expect(reservation_input, validate=True)
    @events_namespace.marshal_with(reservation_model)
    def post(self):
        post_data = request.get_json()
        event_name = post_data.get("event")
        ticket_type = post_data.get("type")

        try:
            event = model.Event.query.filter_by(name=event_name).one()
            ticket_pool = (
                model.TicketPool.query.filter_by(event=event)
                .filter_by(type=ticket_type)
                .one()
            )
            reservation = ticket_pool.reserve_ticket()
            db.session.commit()
        except (MultipleResultsFound, NoResultFound):
            events_namespace.abort(404, "Invalid event or ticket type")
        except model.EmptyTicketPoolError as e:
            events_namespace.abort(404, str(e))
        else:
            return reservation


class Reservations(Resource):
    @events_namespace.marshal_with(reservation_model)
    def get(self, reservation_id):
        try:
            reservation = model.Reservation.query.filter_by(id=reservation_id).one()
        except (MultipleResultsFound, NoResultFound):
            events_namespace.abort(404, "Reservation does not exist.")
        else:
            return reservation


def get_reservation(reservation_id):
    reservation = model.Reservation.query.filter_by(id=reservation_id).one()
    if reservation.has_expired():
        raise model.ReservationExpiredError
    return reservation


payment_input = events_namespace.model(
    "PaymentInput",
    {
        "amount": fields.Integer(required=True),
        "token": fields.String(required=True),
        "currency": fields.String(required=True),
    },
)

payment_result_model = events_namespace.model(
    "PaymentResult",
    {
        "amount": fields.Integer(required=True),
        "currency": fields.String(required=True),
    },
)


class Payment(Resource):
    @events_namespace.expect(payment_input, validate=True)
    @events_namespace.marshal_with(payment_result_model)
    def post(self, reservation_id):
        try:
            reservation = get_reservation(reservation_id)
            post_data = request.get_json()
            amount = post_data.get("amount")
            token = post_data.get("token")
            currency = post_data.get("currency")
            gateway = PaymentGateway()
            result = gateway.charge(amount, token, currency)
        except (MultipleResultsFound, NoResultFound):
            events_namespace.abort(404, "Reservation is not valid!")
        except (CardError, PaymentError, CurrencyError) as e:
            events_namespace.abort(404, f"Payment error: {str(e)}!")
        except model.ReservationExpiredError:
            events_namespace.abort(404, "Reservation has expired!")
        else:
            db.session.delete(reservation)
            db.session.commit()
            return result._asdict()


stats_result_model = events_namespace.model(
    "StatsResult",
    {
        "event": fields.String(required=True),
        "ticket_type": fields.String(required=True),
        "reservations": fields.Integer(required=True),
    },
)


class Statistics(Resource):
    @events_namespace.marshal_with(stats_result_model, as_list=True)
    def get(self):
        ticket_type = "all"
        get_data = request.get_json(silent=True)
        if get_data is not None:
            ticket_type = get_data.get("ticket_type", "all")

        return get_event_statistics(ticket_type)


events_namespace.add_resource(EventList, "/")
events_namespace.add_resource(Event, "/<int:event_id>")
events_namespace.add_resource(ReservationsList, "/reservation")
events_namespace.add_resource(Reservations, "/reservation/<int:reservation_id>")
events_namespace.add_resource(Payment, "/reservation/<int:reservation_id>/payment")
events_namespace.add_resource(Statistics, "/statistics")
