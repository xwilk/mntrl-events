import datetime as dt

from project import db


class EmptyTicketPoolError(Exception):
    pass


class ReservationExpiredError(Exception):
    pass


class Event(db.Model):
    __tablename__ = "events"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(128), nullable=False, unique=True)
    datetime = db.Column(db.DateTime, nullable=False)

    ticket_pools = db.relationship(
        "TicketPool",
        back_populates="event",
        cascade="all,delete",
        order_by="desc(TicketPool.importance)",
    )


class TicketPool(db.Model):
    __tablename__ = "ticket_pools"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    type = db.Column(db.String, nullable=False)
    importance = db.Column(db.Integer, nullable=False)
    available_tickets = db.Column(db.Integer, nullable=False)

    event_id = db.Column(db.ForeignKey("events.id"), nullable=False, index=True)
    event = db.relationship("Event", back_populates="ticket_pools")

    reservations = db.relationship(
        "Reservation", back_populates="ticket_pool", cascade="all,delete"
    )

    def reserve_ticket(self):
        self.release_expired_reservations()
        if self.available_tickets < 1:
            raise EmptyTicketPoolError(
                f"{self.type.capitalize()} tickets are no longer available!"
            )
        reservation = Reservation(self)
        self.reservations.append(reservation)
        self.available_tickets -= 1
        db.session.add(reservation)
        return reservation

    def release_expired_reservations(self):
        for reservation in self.reservations:
            if reservation.has_expired():
                self.reservations.remove(reservation)
                self.available_tickets += 1


class Reservation(db.Model):
    __tablename__ = "reservations"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    expiration_time = db.Column(db.DateTime, nullable=False)
    ticket_pool_id = db.Column(
        db.ForeignKey("ticket_pools.id"), nullable=False, index=True
    )
    ticket_pool = db.relationship("TicketPool")

    def __init__(self, ticket_pool):
        self.ticket_pool = ticket_pool
        self.expiration_time = dt.datetime.now() + dt.timedelta(minutes=15)

    def has_expired(self):
        return dt.datetime.now() > self.expiration_time
