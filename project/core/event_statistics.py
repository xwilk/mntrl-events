from dataclasses import dataclass

from project import db, model


@dataclass
class StatsRow:
    event: str
    reservations: int
    ticket_type: str = "all"


def get_event_statistics(ticket_type="all"):
    stats = (
        db.session.query(
            model.Event.name.label("event"),
            db.func.count(model.Reservation.id).label("reservations"),
        )
        .outerjoin(
            model.TicketPool,
            model.Event.id == model.TicketPool.event_id,
        )
        .outerjoin(
            model.Reservation,
            model.TicketPool.id == model.Reservation.ticket_pool_id,
        )
        .group_by(model.Event.name)
    )

    if ticket_type != "all":
        stats = stats.filter(model.TicketPool.type == ticket_type)

    stats = stats.order_by(model.Event.name)

    return [StatsRow(**row._asdict(), ticket_type=ticket_type) for row in stats]