import datetime

from flask.cli import FlaskGroup

from project import create_app, db
from project.model import Event, TicketPool

app = create_app()
cli = FlaskGroup(create_app=create_app)


@app.shell_context_processor
def make_shell_context():
    return {"db": db, "Event": Event, "TicketPool": TicketPool}


@cli.command("recreate_db")
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command("seed_db")
def seed_db():
    e1 = Event(name="BoringEvent", datetime=datetime.datetime.today())
    db.session.add(e1)
    db.session.add(
        TicketPool(event=e1, type="regular", importance=1, available_tickets=5)
    )

    e2 = Event(name="CoolEvent", datetime=datetime.datetime.today())
    db.session.add(e2)
    db.session.add(
        TicketPool(event=e2, type="regular", importance=1, available_tickets=10)
    )
    db.session.add(
        TicketPool(event=e2, type="premium", importance=20, available_tickets=5)
    )
    db.session.add(TicketPool(event=e2, type="vip", importance=99, available_tickets=2))
    db.session.commit()


if __name__ == "__main__":
    cli()
