## mntrl-events - Recruitment task

Project uses docker containers for development environment.
To bring up docker containers (Windows):
```commandline
$> cd /path/to/project
$> docker-compose up -d --build
```

Prepare database:
```commandline
$> docket-compose exec backend flask db upgrade
```

Populate test data in database:
```commandline
$> docker-compose exec backend python manage.py seed_db
```

Run tests:
```commandline
$> docker-compose exec backend python -m pytest "project/tests"
```

##### Front end

The project can serve as a backend for a frontend service. The API follows RESTful design principles. 
Frontend can use HTTP methods to query the needed data. When containers are running, you can visit api swagger docs at **localhost:5001/doc/** to check available HTTP methods.
 